/*
 * Copyright (c) 2001-2003 Swedish Institute of Computer Science.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT 
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
 * OF SUCH DAMAGE.
 *
 * This file is part of the lwIP TCP/IP stack.
 * 
 * Author: Adam Dunkels <adam@sics.se>
 *
 */

#include "tcpecho.h"

#include "lwip/opt.h"
//#include "MENU/RELOJ.h"
#include "MENU/TASK_CONTROL.h"

#if LWIP_NETCONN

#include "lwip/sys.h"
#include "lwip/api.h"




uint8_t USUARIO[50] = "\n\r     Usuario ";
uint8_t usuario;
/*-----------------------------------------------------------------------------------*/


static void tcpecho_thread_New(void *arg)
{
	struct netconn ** conn = (struct netconn**)arg;
	struct netconn * newconn = *conn;
	err_t err;

	USUARIO[15]= 0x30 + usuario;
  while (1) {

    /*printf("accepted new connection %p\n", newconn);*/
    /* Process the new connection. */
      struct netbuf *buf;
      void *data;
      u16_t len;

      netconn_write(newconn, USUARIO,sizeof(USUARIO), NETCONN_COPY);
      netconn_write(newconn, INIT,sizeof(INIT), NETCONN_COPY);
      netconn_write(newconn, CONTINUE,sizeof(CONTINUE), NETCONN_COPY);


      while (1) {
        /*printf("Recved\n");*/

    	  //netconn_recv(newconn,&buf);

//


    		if((err = netconn_recv(newconn, &buf))== ERR_OK)

    //      	if((netconn_recv(newconn, &buf))== ERR_OK)
    	  do {
    		  for(;;){
    	//	  netbuf_delete(buf);

    		  INIT_MENU(&newconn);
    		  MENU_SELECT(&newconn);

    		  }
        } while (netbuf_next(buf) >= 0);
        netbuf_delete(buf);
      /*printf("Got EOF, looping\n");*/ 
      /* Close connection and discard connection identifier. */

    }
  }
  netconn_close(newconn);
  netconn_delete(newconn);
}

static void  tcpecho_thread(void *arg){
	struct netconn *conn, *newconn[6]={0,0,0,0,0,0};


					I2C_INIT();
					RTC_init();

					//xTaskCreate(RTC_ECO, "RTC_ECO",configMINIMAL_STACK_SIZE,NULL, 3, &RTC_ECO_HANDLER);

	  LWIP_UNUSED_ARG(arg);

	  /* Create a new connection identifier. */
	  /* Bind connection to well known port number 7. */
	#if LWIP_IPV6
	  conn = netconn_new(NETCONN_TCP_IPV6);
	  netconn_bind(conn, IP6_ADDR_ANY, 7);
	#else /* LWIP_IPV6 */
	  conn = netconn_new(NETCONN_TCP);
	  netconn_bind(conn, IP_ADDR_ANY, 50000);
	#endif /* LWIP_IPV6 */
	  LWIP_ERROR("tcpecho: invalid conn", (conn != NULL), return;);

	  /* Tell connection to go into listening mode. */
	  netconn_listen(conn);
      int i=0;

	  xTaskCreate(HORA_ECO,"HORA_ECO", configMINIMAL_STACK_SIZE, NULL, 3,&HORA_ECO_HANDLE );

	  while (1) {

	    /* Grab new connection. */
	    netconn_accept(conn, &newconn[i]);


	    CLIENTS[i] = newconn[i];
	    usuario = i+1;
	    /*printf("accepted new connection %p\n", newconn);*/
	    /* Process the new connection. */

	    sys_thread_new("tcpecho_thread_New", tcpecho_thread_New, (&newconn[i]), DEFAULT_THREAD_STACKSIZE, DEFAULT_THREAD_PRIO);
	    i++;


	  }
}
/*-----------------------------------------------------------------------------------*/
void
tcpecho_init(void)
{
  sys_thread_new("tcpecho_thread", tcpecho_thread, NULL, DEFAULT_THREAD_STACKSIZE, DEFAULT_THREAD_PRIO);

}
/*-----------------------------------------------------------------------------------*/

#endif /* LWIP_NETCONN */
