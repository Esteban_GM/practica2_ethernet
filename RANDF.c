/*
 * RANDF.c
 *
 *  Created on: 11/04/2017
 *      Author: esteban
 */
#include "RANDF.h"
#include "UART.h"
#include "TASK_CONTROL.h"
#include "CONFIG.h"


void CLEAR_SCREEN(void * pvParameters){
	UART_Type *base = pvParameters;
	UART_WriteBlocking(base,(void *)"\033[2J",sizeof("\033[2J"));
}

void GPIO_delay(uint32_t delay)
{
	uint32_t counter;
	for(counter=delay;counter>0;counter--)
	{
	}
}
/*
uint8_t X;
uint8_t Y;
void CURSOR_XY(int X,int Y){

	UART_WriteBlocking(UART0,(void *)"\033[1;5H",sizeof("\033[1;5H"));
}
*/
