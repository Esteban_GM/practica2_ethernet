/*
 * I2CMEM.h
 *
 *  Created on: 12/04/2017
 *      Author: esteban
 */

#ifndef SOURCE_I2CMEM_H_
#define SOURCE_I2CMEM_H_

/** FreeRTOS drivers includes. */
#include "fsl_port.h"
#include "fsl_i2c_freertos.h"

/** FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"
#include "semphr.h"
#include "stdio.h"
#include "timers.h"


/** Memory slave address of the 24lc256**/
#define SLAVE_ADDR_24LC256 (0x50)

/** Variables**/
uint8_t I2C_REG_MEM;

uint32_t MENSAJE;
uint32_t ADDR;
uint8_t i2c_mem_reg;
uint8_t I2C_BUFFER[255];

i2c_rtos_handle_t RTC_I2C_HANDLE;
i2c_master_transfer_t RTC_I2C;

//i2c_master_handle_t I2C_TRANSFER_HANDLE;
i2c_rtos_handle_t I2C_TRANSFER_HANDLE;
i2c_master_transfer_t I2C_TRANSFER;

void I2C_INIT(void);
void I2C_WRITE(void *arg);
void I2C_READ(void *arg);

#endif /* SOURCE_I2CMEM_H_ */
