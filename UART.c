/*
 * UART.c
 *
 *  Created on: 04/04/2017
 *      Author: esteban
 */


#include "UART.h"


static uart_rtos_handle_t UART0_RTOS_TT_HANDLE;	/** UART TERATERM HANDLE**/
uart_handle_t UART0_TT_HANDLE;


uart_config_t UART0_TERATERM; 	/** TERATERM**/
uart_config_t UART3_TERATERM; 	/** TERATERM**/

uint8_t BUFFER_TERATERM[100];				/** Tama�o del buffer de la UART**/
uint8_t buffer_MENU[1];



void UART_TERATERM_INIT(){

	/** Configuracion inicial de la UART**/
	UART0_TERATERM.baudRate_Bps = 9600;
	UART0_TERATERM.enableRx = true;
	UART0_TERATERM.enableTx = true;
	UART0_TERATERM.parityMode = kUART_ParityDisabled;
	UART0_TERATERM.stopBitCount = kUART_OneStopBit;
	UART0_TERATERM.rxFifoWatermark = false;
	UART0_TERATERM.txFifoWatermark = false;

	/** Frecuencia del reloj **/
	uint32_t UART0_CLK_TERATERM = CLOCK_GetFreq(UART0_CLK_SRC);

	/** Enable clocks for UART */
	CLOCK_EnableClock(kCLOCK_Uart0);
	CLOCK_EnableClock(kCLOCK_PortB);


	/** Configuracion de los pines UART TERATERM**/
	PORT_SetPinMux(PORTB, 16, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTB, 17, kPORT_MuxAlt3);

	UART_Init(UART0, &UART0_TERATERM, UART0_CLK_TERATERM);

	/** Interrupt Enable**/
	UART_EnableInterrupts(UART0,kUART_IdleLineInterruptEnable);
	/** Set UART interrupt priority */
	NVIC_SetPriority(UART0_RX_TX_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
	/** Enable interrupt in the NVIC */
	NVIC_EnableIRQ(UART0_RX_TX_IRQn);



}
void UART_BLUETOOTH_INIT(){

	/** Configuracion inicial de la UART**/
	UART3_TERATERM.baudRate_Bps = 9600;
	UART3_TERATERM.enableRx = true;
	UART3_TERATERM.enableTx = true;
	UART3_TERATERM.parityMode = kUART_ParityDisabled;
	UART3_TERATERM.stopBitCount = kUART_OneStopBit;
	UART3_TERATERM.rxFifoWatermark = false;
	UART3_TERATERM.txFifoWatermark = false;

	/** Frecuencia del reloj **/
	uint32_t UART3_CLK_TERATERM = CLOCK_GetFreq(UART3_CLK_SRC);

	/** Enable clocks for UART */
	CLOCK_EnableClock(kCLOCK_Uart3);
	CLOCK_EnableClock(kCLOCK_PortB);


	/** Configuracion de los pines UART TERATERM**/
	PORT_SetPinMux(PORTB, 10, kPORT_MuxAlt3);
	PORT_SetPinMux(PORTB, 11, kPORT_MuxAlt3);

	UART_Init(UART3, &UART3_TERATERM, UART3_CLK_TERATERM);

	/** Interrupt Enable**/
	UART_EnableInterrupts(UART3,kUART_IdleLineInterruptEnable);
	/** Set UART interrupt priority */
	NVIC_SetPriority(UART3_RX_TX_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);
	/** Enable interrupt in the NVIC */
	NVIC_EnableIRQ(UART3_RX_TX_IRQn);



}


void UART0_TERATERM_ENVIAR(uint8_t DATA[100])
{
		UART_RTOS_Send(&UART0_RTOS_TT_HANDLE, DATA, 100);
}



uint8_t UART0_TERATERM_RECIBIR_MENU()
{
	size_t received;
	UART_RTOS_Receive(&UART0_RTOS_TT_HANDLE, buffer_MENU, sizeof(buffer_MENU), &received);
	if (received > 0)
	{
		/* send back the received data */
		UART_RTOS_Send(&UART0_RTOS_TT_HANDLE, (uint8_t*)buffer_MENU, received);
	}
	return buffer_MENU[0];
}

