/*
 * TASK_CONTROL.h
 *
 *  Created on: 05/04/2017
 *      Author: esteban
 */

#ifndef SOURCE_TASK_CONTROL_H_
#define SOURCE_TASK_CONTROL_H_

#include "CONFIG.h"

uint8_t INIT[100] 	= "\n\r Inicializando Sistema";
uint8_t CONTINUE[100] 	= "\n\r Presione ENT para continuar";

uint8_t MENU_0[100] = "\n\r MENU PRINCIPAL ";
uint8_t MENU_1[100] = "\n\r 1.-  Leer Memoria I2C ";
uint8_t MENU_2[100] = "\n\r 2.-  Escribir Memoria I2C";
uint8_t MENU_3[100] = "\n\r 3.-  Fijar Hora";
uint8_t MENU_4[100] = "\n\r 4.-  Fijar Fecha";
uint8_t MENU_5[100] = "\n\r 5.-  Formato de hora";
uint8_t MENU_6[100] = "\n\r 6.-  Leer Hora";
uint8_t MENU_7[100] = "\n\r 7.-  Leer Fecha";
uint8_t MENU_8[100] = "\n\r 8.-  Comunicacion con terminal 2";
uint8_t MENU_9[100] = "\n\r 9.-  LCD ECO \n";
/** Task1**/
uint8_t MEM_R[100] = "\n\r Opcion 1: Leer Memoria \n";
uint8_t READ_DIR[100] = "Direccion de lectura (hexadecimal): 0x";
uint8_t DIR_ERROR[100] = "Direccion de lectura Incompleta";
uint8_t LONG_ERROR[100] = "Error de Longitud de lectura";

/**Task2**/

uint8_t ESCRIBIR[100] = "\n\r Opcion 2:  Escribir en la memoria";
uint8_t ADDRES[100] 	  = "\n\r Direccion a escribir: 0x";

/** Task3 **/
uint8_t GET_HOUR[100] = "\n\r Opcion 3:  Escribir la hora en hh/mm/ss: ";
uint8_t GET_CONFIRM[100] 	  = "\n\r HORA escrita correctamente";

/** Task4 **/
uint8_t GET_DATE[100] = "\n\r Opcion 4:  Escribir la FECHA en dd/mm/aa: ";
uint8_t DATE_SUCCESS[100] 	  = "\n\r FECHA escrita correctamente";

/** Task5 **/
uint8_t GET_FORMAT[100] = "\n\r Opcion 5:  Escribir 1 para un formato de 12 horas\n\r     Escribe 2 para un formato de 24 horas ";
uint8_t FORMAT_SUCC[100] 	  = "\n\r Formato escrito correctamente";
uint8_t FORMAT_FAIL[100] 	  = "\n\r ERROR: Seleccion incorrecta de formato";
/** Task6 **/
uint8_t GET_HORA[100] = "\n\r Opcion 6: La hora acutal es :\n\r";
/** Task7 **/
uint8_t GET_FECHA[100] = "\n\r Opcion 7: La fecha acutal es :\n\r";
/** Task8 **/
uint8_t CHAT[100] = "\n\r Opcion 8: Chat entre terminales";



/** Task9 **/
uint8_t ECO[100] = "\n\r Opcion 9:  ECO en LCD:  \n   ";

//uint8_t *volatile I2C_BUFFER[255];

/** Enumeration to define the menus. */
enum
{
	MENU0,
	MENU1,
	MENU2,
	MENU3,
	MENU4,
	MENU5,
	MENU6,
	MENU7,
	MENU8,
	MENU9
};

/** PRIORIDADES**/
//#define Menu_task_PRIORITY (1)
//TaskHandle_t xTaskClock = NULL;

void INIT_MESSAGE(void);
void INIT_MENU(void *arg);
void MENU_SELECT(void *arg);

void TASK_1(void *arg);
void TASK_2(void *arg);
void TASK_3(void *arg);
void TASK_4(void *arg);
void TASK_5(void *arg);
void TASK_6(void *arg);
void TASK_7(void *arg);
void TASK_8(void *arg);
void TASK_9(void *arg);

void RTC_READ(void *arg);
void RTC_WRITE(void *arg);
void RTC_ECO(void);
TaskHandle_t RTC_ECO_HANDLER = NULL;



#endif /* SOURCE_TASK_CONTROL_H_ */
