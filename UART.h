/*
 * UART.h
 *
 *  Created on: 04/04/2017
 *      Author: esteban
 */

#ifndef SOURCE_UART_H_
#define SOURCE_UART_H_


#include "fsl_uart.h"
#include "fsl_port.h"
#include "fsl_uart_freertos.h"
#include "task.h"


void UART_TERATERM_INIT(void);
void UART_BLUETOOTH_INIT(void);
void UART0_TERATERM_ENVIAR(uint8_t DATA[100]);
uint8_t UART0_TERATERM_RECIBIR_MENU();

#endif /* SOURCE_UART_H_ */
