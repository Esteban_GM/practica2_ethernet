/*
 * RELOJ.h
 *
 *  Created on: 15/04/2017
 *      Author: esteban
 */

#ifndef SOURCE_RELOJ_H_
#define SOURCE_RELOJ_H_
/** FreeRTOS drivers includes.*/
#include "fsl_uart.h"
#include "fsl_rtc.h"
#include "fsl_port.h"

/** FreeRTOS kernel includes.*/
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "queue.h"
#include "semphr.h"
#include "stdio.h"
#include "timers.h"
#include "MK64F12.h"
#include "lwip/sys.h"
#include "lwip/api.h"

/** Variables. */
rtc_datetime_t DateTime;
rtc_datetime_t SetTime;


uint8_t ANIO = 17; /*!< Range from 1970 to 2099.*/
uint8_t	MES = 04;/*!< Range from 1 to 12.*/
uint8_t DIA = 15;  /*!< Range from 1 to 31 (depending on month).*/
uint8_t HORA = 6;  /*!< Range from 0 to 23.*/
uint8_t MINUTO = 28; /*!< Range from 0 to 59.*/
uint8_t SEGUNDO  = 35;  /*!< Range from 0 to 59.*/
uint32_t RTC_SUB_ADDR;
extern uint8_t BANDERA_RTC;
uint8_t BANDERA_H_F;
/** Constant containing the RTC address */
#define RTC_ADDR  (0x6FU)
#define RTC_ST_MASK (0x80U)
//status_t setRTC_VAL(void);
uint8_t HORA_BUFFER[7];

/*
i2c_rtos_handle_t RTC_I2C_HANDLE;
i2c_master_transfer_t RTC_I2C;
*/
uint8_t RTC_VAL;
/******************************************************************************/
/*!
 	 \brief This function is the ISR for the real time clock interruption.
 	 \param[in] Void.
 	 \return Void.
 */
/******************************************************************************/
void RTC_init(void);
void RTC_init(void);

#endif /* SOURCE_RELOJ_H_ */
