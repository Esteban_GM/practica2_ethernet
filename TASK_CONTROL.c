/*
 * TASK_CONTROL.c
 *
 *  Created on: 05/04/2017
 *      Author: esteban
 */

//#include "CONFIG.h"
//#include "UART.h"
//#include "RANDF.h"
//#include "I2CMEM.h"
//#include "RELOJ.h"
//#include "LCDSPI.h"
#include "MK64F12.h"
#include "tcpecho/tcpecho.h"

//#include "tcpecho.h"

//#include "lwip/opt.h"
//#include "MENU/RELOJ.h"
#include "MENU/TASK_CONTROL.h"
#include "MENU/RELOJ.h"
#include "MENU/I2CMEM.h"
#include "MENU/LCDSPI.h"

#include "lwip/opt.h"
#include "lwip/sys.h"
#include "lwip/api.h"

uint8_t BANDERA_RTC = 1;
uint8_t FLAG_11[10];

xSemaphoreHandle GateKeeper = 0;

/** Menu inicial**/
void INIT_MENU(void *arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
//    struct netbuf *buf;

//	err_t err;
	GateKeeper = xSemaphoreCreateMutex();

	/****************************************/
	/*********** MENU PRINCIPAL *************/
	/****************************************/
	netconn_write(newconn, MENU_0, sizeof(MENU_0), NETCONN_COPY);
	netconn_write(newconn, MENU_1, sizeof(MENU_1), NETCONN_COPY);
	netconn_write(newconn, MENU_2, sizeof(MENU_2), NETCONN_COPY);
	netconn_write(newconn, MENU_3, sizeof(MENU_3), NETCONN_COPY);
	netconn_write(newconn, MENU_4, sizeof(MENU_4), NETCONN_COPY);
	netconn_write(newconn, MENU_5, sizeof(MENU_5), NETCONN_COPY);
	netconn_write(newconn, MENU_6, sizeof(MENU_6), NETCONN_COPY);
	netconn_write(newconn, MENU_7, sizeof(MENU_7), NETCONN_COPY);
	netconn_write(newconn, MENU_8, sizeof(MENU_8), NETCONN_COPY);
	netconn_write(newconn, MENU_9, sizeof(MENU_9), NETCONN_COPY);

}

void MENU_SELECT(void *arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buf;
	void *data;
	uint16_t len;
	err_t err;

	err = netconn_recv(newconn, &buf);

	netbuf_data(buf, &data, &len);
	uint8_t SW = ((char*) data)[0];
	SW = SW - 0x30;
//	netconn_write(CLIENTS[1], (void * )data,
//				sizeof(data), NETCONN_COPY);
	switch (SW) {
	case MENU1:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 1 seleccionado",
				sizeof("\n\r\rMenu 1 seleccionado"), NETCONN_COPY);
		TASK_1(&newconn);

		break;
	case MENU2:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 2 seleccionado",
				sizeof("\n\r\rMenu 2 seleccionado"), NETCONN_COPY);
		TASK_2(&newconn);

		break;
	case MENU3:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 3 seleccionado",
				sizeof("\n\r\rMenu 3 seleccionado"), NETCONN_COPY);
		TASK_3(&newconn);

		break;
	case MENU4:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 4 seleccionado",
				sizeof("\n\r\rMenu 4 seleccionado"), NETCONN_COPY);
		TASK_4(&newconn);

		break;
	case MENU5:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 5 seleccionado",
				sizeof("\n\r\rMenu 5 seleccionado"), NETCONN_COPY);
		TASK_5(&newconn);

		break;

	case MENU6:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 6 seleccionado",
				sizeof("\n\r\rMenu 6 seleccionado"), NETCONN_COPY);
		TASK_6(&newconn);
		break;
	case MENU7:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 7 seleccionado",
				sizeof("\n\r\rMenu 7 seleccionado"), NETCONN_COPY);
		TASK_7(&newconn);

		break;
	case MENU8:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 8 seleccionado",
				sizeof("\n\r\rMenu 8 seleccionado"), NETCONN_COPY);
		TASK_8(&newconn);

		break;
	case MENU9:
		netbuf_delete(buf);
		netconn_write(newconn, (void * )"\n\r\rMenu 9 seleccionado",
				sizeof("\n\r\rMenu 9 seleccionado"), NETCONN_COPY);
		TASK_9(&newconn);

		break;

	default:
		break;
	}
}

void TASK_1(void * arg) {

	//	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buff;
	void *data;
	uint16_t len;
	err_t err;

	uint16_t Counter;
	ADDR = 0;

	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) MEM_R, sizeof(MEM_R), NETCONN_COPY); //  uint8_t MEM_R[100] = "\n\r Opcion 1: Leer Memoria \n";
	netconn_write(newconn, (void * ) READ_DIR, sizeof(READ_DIR), NETCONN_COPY); //	    uint8_t READ_DIR[100] = "Direccion de lectura (hexadecimal): 0x";
	vTaskDelay(100);

//	    uint8_t DIR_ERROR[100] = "Direccion de lectura Incompleta";
//	    uint8_t LONG_ERROR[100] = "Error de Longitud de lectura";

	/** Escribimos la direccion de la memoria a leer*/

	netconn_recv(newconn, &buff);

	netbuf_data(buff, &data, &len);

	for (Counter = 0; Counter < 4; Counter++) {
		uint8_t SW = ((char*) data)[Counter];
		SW = SW - 0x30;
		ADDR = ADDR | (SW << (12 - (4 * Counter)));
	}
	netbuf_delete(buff);

	netconn_write(newconn, (void * ) "Longitud de bytes a leer",
			sizeof("Longitud de bytes a leer"), NETCONN_COPY); //	    uint8_t READ_DIR[100] = "Direccion de lectura (hexadecimal): 0x";

	err = netconn_recv(newconn, &buff);

	//			/** Espera a que se escriba que tan largo va a ser el MENSAJE EN BYTES*/
	netbuf_data(buff, &data, &len);

	MENSAJE = 0;
	for (Counter = 0; Counter < 2; Counter++) {
		uint8_t SW = ((char*) data)[Counter];
		SW = SW - 0x30;
		MENSAJE = MENSAJE | (SW << (4 - (4 * Counter)));

	}

	netbuf_delete(buff);
	for (;;) {
		if (xSemaphoreTake(GateKeeper, 1000)) {
			I2C_READ(&newconn);
			xSemaphoreGive(GateKeeper);
			vTaskDelay(100);
			break;
		}
	}

}

void TASK_2(void * arg) {
	uint8_t Counter = 0;
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	//  struct netbuf *buff; = netbuf_new;
	struct netbuf *buff;
	void *data;
	uint16_t len;
	err_t err;

	/** Reseteamos la address  y la longitud del mensaje junto con el counter**/
	ADDR = 0;
	MENSAJE = 0;
	Counter = 0;

	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) ESCRIBIR, sizeof(ESCRIBIR), NETCONN_COPY); //  uint8_t MEM_R[100] = "\n\r Opcion 1: Leer Memoria \n";
	netconn_write(newconn, (void * ) ADDRES, sizeof(ADDRES), NETCONN_COPY); //	    uint8_t READ_DIR[100] = "Direccion de lectura (hexadecimal): 0x";
	vTaskDelay(100);

	/** Tarea bloqueada, en espera de 4 caracteres */

	err = netconn_recv(newconn, &buff);
//	netconn_set_recvtimeout
	netbuf_data(buff, &data, &len);
	ADDR = 0;
	for (Counter = 0; Counter < 4; Counter++) {
		uint8_t SW = ((char*) data)[Counter];
//		SW = SW - 0x30;
		ADDR = ADDR | ((SW - 0x30) << (12 - (4 * Counter)));
	}
	netbuf_delete(buff);

	netconn_write(newconn, (void * ) "Texto a guardar: ",
			sizeof("Texto a guardar: "), NETCONN_COPY); //	    uint8_t READ_DIR[100] = "Direccion de lectura (hexadecimal): 0x";
	vTaskDelay(100);

	err = netconn_recv(newconn, &buff);

	netbuf_data(buff, &data, &len);

	for (Counter = 0; Counter < 100; Counter++) {
		I2C_BUFFER[Counter] = ((char *) data)[Counter];
	}

	netbuf_delete(buff);

	for (;;) {
		if (xSemaphoreTake(GateKeeper, 1000)) {
			I2C_WRITE(&newconn);
			xSemaphoreGive(GateKeeper);
			vTaskDelay(100);
			break;
		}
	}

}

void TASK_3(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buff;
	void *data;
	uint16_t len;
	err_t err;
	uint8_t Counter = 0;
	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) GET_HOUR, sizeof(GET_HOUR), NETCONN_COPY);
	/** Tarea bloqueada, en espera de 4 caracteres */

	err = netconn_recv(newconn, &buff);

	netbuf_data(buff, &data, &len);

	for (Counter = 0; Counter < 6; Counter++) {
		uint8_t SW = ((char*) data)[Counter];
		SW = SW - 0x30;
		HORA_BUFFER[Counter] = SW;
	}

	/** ACOMODA LOS DATOS PARA ESCRIBIRLOS **/
	HORA = (((HORA_BUFFER[0] << 4) & 0x30) + (HORA_BUFFER[1]));
	MINUTO = (((HORA_BUFFER[2] << 4) & 0x70) + (HORA_BUFFER[3]));
	SEGUNDO = (((HORA_BUFFER[4] << 4) & 0x70) + (HORA_BUFFER[5]));
	HORA_BUFFER[0] = SEGUNDO + 0x80;
	HORA_BUFFER[1] = MINUTO;
	HORA_BUFFER[2] = HORA;
	for (;;) {
		if (xSemaphoreTake(GateKeeper, 1000)) {
			RTC_WRITE(&newconn);
			netconn_write(newconn, (void * ) GET_CONFIRM, sizeof(GET_CONFIRM),
					NETCONN_COPY);
			xSemaphoreGive(GateKeeper);
			vTaskDelay(100);
			break;
		}
	}
	netbuf_delete(buff);

}

void TASK_4(void * arg) {
	//	/** Declaramos las variables que vamos a necesitar en esta tarea**/
	uint8_t Counter = 0;

	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buff;
	void *data;
	uint16_t len;
	err_t err;

	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) GET_DATE, sizeof(GET_DATE), NETCONN_COPY);
	/** Tarea bloqueada, en espera de 4 caracteres */
	err = netconn_recv(newconn, &buff);

	netbuf_data(buff, &data, &len);

	for (Counter = 0; Counter < 6; Counter++) {
		uint8_t SW = ((char*) data)[Counter];
		SW = SW - 0x30;
		HORA_BUFFER[Counter] = SW;
	}
	/** ACOMODA LOS DATOS PARA ESCRIBIRLOS **/
	DIA = (((HORA_BUFFER[0] << 4) & 0x30) + (HORA_BUFFER[1]));
	MES = (((HORA_BUFFER[2] << 4) & 0x30) + (HORA_BUFFER[3]));
	ANIO = (((HORA_BUFFER[4] << 4)) + (HORA_BUFFER[5]));
	HORA_BUFFER[0] = DIA;
	HORA_BUFFER[1] = MES;
	HORA_BUFFER[2] = ANIO;

	/** Mandamos escribir la hora **/
	RTC_SUB_ADDR = 0x04;
	for (;;) {
		if (xSemaphoreTake(GateKeeper, 1000)) {
			RTC_WRITE(&newconn);
			netconn_write(newconn, (void * ) DATE_SUCCESS, sizeof(DATE_SUCCESS),
					NETCONN_COPY);
			xSemaphoreGive(GateKeeper);
			vTaskDelay(100);
			break;
		}
	}

}
void TASK_5(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buf;
	status_t status;
	void *data;
	uint16_t len;
	err_t err;
	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) GET_FORMAT, sizeof(GET_FORMAT),
			NETCONN_COPY);
	RTC_SUB_ADDR = 0x02; //Comienza con las horas

//		//		uint8_t FORMAT_SUCC[100] 	  = "\n\r Formato escrito correctamente";
//		//		uint8_t FORMAT_FAIL[100] 	  = "\n\r ERROR: Seleccion incorrecta de formato";
//
	err = netconn_recv(newconn, &buf);

	netbuf_data(buf, &data, &len);
	uint8_t SW = ((char*) data)[0];
	SW = SW - 0x30;

	BANDERA_RTC = 2;

	RTC_SUB_ADDR = 0x02;
	uint8_t x = 0x00;
	I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
	I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
	I2C_TRANSFER.direction = kI2C_Read;
	I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
	I2C_TRANSFER.subaddressSize = 1;
	I2C_TRANSFER.data = &x;
	I2C_TRANSFER.dataSize = 1;
	vTaskDelay(10);
	status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
	vTaskDelay(10);

	if (SW == 1) {
		BANDERA_RTC = 1;
		RTC_SUB_ADDR = 0x02;

		x = x | 0x40;

		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
		I2C_TRANSFER.direction = kI2C_Write;
		I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &x;
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay(10);
		status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		vTaskDelay(10);
	} else if (SW == 2) {
		BANDERA_RTC = 2;
		RTC_SUB_ADDR = 0x02;
		x = x & 0xBF;
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
		I2C_TRANSFER.direction = kI2C_Write;
		I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &x;
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay(10);
		status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		vTaskDelay(10);

	} else
		return;
	BANDERA_H_F = 0;
	RTC_SUB_ADDR = 0x00; //Comienza con las horas
	BANDERA_H_F = 0;
	for (;;) {
		if (xSemaphoreTake(GateKeeper, 1000)) {
			netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
					sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
			RTC_READ(&newconn);
			xSemaphoreGive(GateKeeper);
			vTaskDelay(100);
		}
	}

}
void TASK_6(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buff;
	void *data;

	uint16_t len;
	err_t err;

	RTC_SUB_ADDR = 0x00; //Comienza con las horas
	BANDERA_H_F = 0;
	if (xSemaphoreTake(GateKeeper, 1000)) {
		netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
				sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
		netconn_write(newconn, (void * ) GET_HORA, sizeof(GET_HORA),
				NETCONN_COPY);
		RTC_READ(&newconn);
		xSemaphoreGive(GateKeeper);
		vTaskDelay(100);
	}
	err = netconn_recv(newconn, &buff);
	netbuf_data(buff, &data, &len);
	netbuf_delete(buff);

}

void TASK_7(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buff;
	void *data;

	uint16_t len;
	err_t err;

	RTC_SUB_ADDR = 0x00; //Comienza con las horas
	BANDERA_H_F = 1;
	if (xSemaphoreTake(GateKeeper, 1000)) {
		netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
				sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
		netconn_write(newconn, (void * ) GET_FECHA, sizeof(GET_FECHA),
				NETCONN_COPY);
		RTC_READ(&newconn);
		xSemaphoreGive(GateKeeper);
		vTaskDelay(100);
	}
	err = netconn_recv(newconn, &buff);
	netbuf_data(buff, &data, &len);
	netbuf_delete(buff);

}
void TASK_8(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buf;
	void *data;
	uint8_t C[2];

	uint16_t len;
	err_t err;
	uint8_t counter = 0;
	uint8_t chat = 0;
	uint8_t broadcast;
	//menu 8  escriba B para hablar con todos los usuarios conectados
	//escriba el numero de usuario para hablar con el



	C[1] = 0;
	err = netconn_recv(newconn, &buf);
	netbuf_data(buf, &data, &len);

	broadcast = ((char*) data)[0];
	netbuf_delete(buf);
	for (counter = 0; counter < 6; counter++) {

					if (CLIENTS[counter] == newconn)	{
						FLAG_11[counter] = 1;
					}
				}


	if ((broadcast - 0x30) == 9) {
		while (C[1] != '*') {

			for (counter = 0; counter < 6; counter++) {

				if (CLIENTS[counter] == newconn)	{
					FLAG_11[counter] = 1;
				}
			}


			err = netconn_recv(newconn, &buf);
			netbuf_data(buf, &data, &len);
			C[1] = ((char *) data)[0];

			for (counter = 0; counter < 9; counter++) {
				if (CLIENTS != NULL)
					netconn_write(CLIENTS[counter], (void * ) data,
							sizeof(data), NETCONN_COPY);
			}
		}
		netbuf_delete(buf);

	} else if ((broadcast - 0x30) <= 9 || (broadcast - 0x30) > 0) {
		while (C[1] != '*') {

			err = netconn_recv(newconn, &buf);
			netbuf_data(buf, &data, &len);
			C[1] = ((char *) data)[0];

			if (FLAG_11[(broadcast - 0x30) == 1]) {
				netconn_write(CLIENTS[(broadcast - 0x30)], (void * )data,
						sizeof(data), NETCONN_COPY);
				netconn_write(newconn, (void * )data, sizeof(data),
						NETCONN_COPY);
			}
		else {
			netconn_write(newconn, (void * )"Usuario no disponible",
					sizeof("Usuario no disponible"), NETCONN_COPY);

		}
		netbuf_delete(buf);
		}
	}
	for (counter = 0; counter < 9; counter++) {
		if (CLIENTS[counter] == newconn) {
			FLAG_11[counter] = 0;
		}
	}

}
void TASK_9(void * arg) {
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;
	struct netbuf *buf;
	void *data;
	uint16_t len;
	err_t err;
	vTaskSuspend(HORA_ECO_HANDLE);
	netconn_write(newconn, (void * )"\n\n\n\n\n\n\n\n\n\n",
			sizeof("\n\n\n\n\n\n\n\n\n\n"), NETCONN_COPY);
	netconn_write(newconn, (void * ) ECO, sizeof(ECO), NETCONN_COPY);
	uint8_t C[2];

	LCDNokia_init();
	LCDNokia_clear();
	LCDNokia_gotoXY(0x10, 0x10);
	vTaskDelay(100);
	C[1] = 0;
	while (C[1] != '*') {

		err = netconn_recv(newconn, &buf);
		netbuf_data(buf, &data, &len);
		I2C_BUFFER[0] = 0;
		uint8_t Counter = 0;
		C[0] = I2C_BUFFER[0];
		for (Counter = 0; Counter < 50; Counter++) {
			I2C_BUFFER[Counter] = 0;
		}
		Counter = 0;
		while (C[0] != 13) {
			I2C_BUFFER[Counter] = ((char *) data)[Counter];
//			C[0]=I2C_BUFFER[Counter];
			C[0] = ((char *) data)[Counter + 1];
			C[1] = ((char *) data)[Counter];

			Counter++;

			//			if (C[0] == '*') C[1] = C[0];
		}
		netbuf_delete(buf);

//		LCDNokia_sendString(data);
		LCD_delay();
		LCDNokia_sendString(I2C_BUFFER);
		LCD_delay();

	}
	vTaskResume(HORA_ECO_HANDLE);
	C[1] = 0;
}

void HORA_ECO(void) {

	LCDNokia_init();
	LCDNokia_clear();
	LCDNokia_gotoXY(0x10, 0x10);
	vTaskDelay(100);

	for (;;) {

		for (;;) {
			RTC_SUB_ADDR = 0x00; //Comienza con las horas
			BANDERA_H_F = 0;
			for (;;) {
					RTC_ECO();
					vTaskDelay(100);
				}
			}
		}
	}



//void TASK_8(void * pvParams) {
//	EventBits_t xBits;
//	uint8_t Counter;
//	uint8_t ITEM_QUEUE;
//	uint8_t BUFF[150];
//	UART_Type *base = (UART_Type *) pvParams;
//	UART_Type *base2;
//	FLAG_11[9] = 0;
//	if (UART0 == base) {
//		base2 = UART3;
//	} else if (UART3 == base) {
//		base2 = UART0;
//	}
//
//	for (;;) {
////		CLEAR_SCREEN();
//
//		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
//		UART_WriteBlocking(base, (void *) CHAT, sizeof(CHAT));
//		//	xBits = xEventGroupWaitBits(Evento, ESC | ENT | CHAR, pdTRUE, pdFALSE, portMAX_DELAY);
//
//		for (;;) {
//
//			for (Counter = 0; Counter < 150; Counter++) {
//				xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC | ENT | CHAR,
//				pdTRUE, pdFALSE, portMAX_DELAY);
//				if (ESC == (ESC & xBits))
//					break;
//				else if (ENT == (ENT & xBits))
//					break;
//			}
//
//			if (ESC == (ESC & xBits))
//				break;
//
////			  	/** Limpiamos el Enter o el ESZ que usamos para salir de la escritura del mensaje**/
//			xEventGroupClearBits(TERATERM_EVENTS,
//			ALL | CHAR | ESC | ENT | DONE);
////
////			  	/** Cálculamos la longuitud del mensaje */
//			MENSAJE = uxQueueMessagesWaiting(TERATERM_QUEUE);
////
////			  	/** Guardamos lo que se escribio en un buffer que luego mandaremos a escribir en la memoria */
//			for (Counter = 0; Counter < MENSAJE; Counter++) {
//
//				xQueueReceive(TERATERM_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
//				//				UART_WriteBlocking(base2,(void *)(ITEM_QUEUE+0x30),sizeof(ITEM_QUEUE+0x30));
//				BUFF[Counter] = ITEM_QUEUE;
//			}
//
//			UART_WriteBlocking(base2, (void *) BUFF, sizeof(BUFF));
////
//
//		}
//		xBits = xEventGroupWaitBits(TERATERM_EVENTS, ESC | ENT, pdTRUE, pdFALSE,
//		portMAX_DELAY);
//		xEventGroupClearBits(TERATERM_EVENTS, ESC | ENT | DONE);
//		if (base == UART0)
//			vTaskResume(INIT_MENU_HANDLE);
//		if (base == UART3)
//			vTaskResume(INIT_MENU_HANDLE);
//
//	}
//}
//void TASK_8_BT(void * pvParams) {
//	EventBits_t xBits;
//	uint8_t Counter;
//	uint8_t ITEM_QUEUE;
//	uint8_t BUFF[150];
//	UART_Type *base = (UART_Type *) pvParams;
//	UART_Type *base2;
//	FLAG_11[9] = 1;
//	if (UART0 == base) {
//		base2 = UART3;
//	} else if (UART3 == base) {
//		base2 = UART0;
//	}
//
//	for (;;) {
////		CLEAR_SCREEN();
//
//		UART_WriteBlocking(base, (void *) "\033[1;10H", sizeof("\033[1;10H"));
//		UART_WriteBlocking(base, (void *) CHAT, sizeof(CHAT));
//		//	xBits = xEventGroupWaitBits(Evento, ESC | ENT | CHAR, pdTRUE, pdFALSE, portMAX_DELAY);
//
//		for (;;) {
//
//			for (Counter = 0; Counter < 150; Counter++) {
//				xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC | ENT | CHAR,
//				pdTRUE, pdFALSE, portMAX_DELAY);
//				if (ESC == (ESC & xBits))
//					break;
//				else if (ENT == (ENT & xBits))
//					break;
//			}
//
//			if (ESC == (ESC & xBits))
//				break;
//
////			  	/** Limpiamos el Enter o el ESZ que usamos para salir de la escritura del mensaje**/
//			xEventGroupClearBits(BLUETOOTH_EVENTS,
//			ALL | CHAR | ESC | ENT | DONE);
////
////			  	/** Cálculamos la longuitud del mensaje */
//			MENSAJE = uxQueueMessagesWaiting(BLUETOOTH_QUEUE);
////
////			  	/** Guardamos lo que se escribio en un buffer que luego mandaremos a escribir en la memoria */
//			for (Counter = 0; Counter < MENSAJE; Counter++) {
//
//				xQueueReceive(BLUETOOTH_QUEUE, &ITEM_QUEUE, portMAX_DELAY);
//				//				UART_WriteBlocking(base2,(void *)(ITEM_QUEUE+0x30),sizeof(ITEM_QUEUE+0x30));
//				BUFF[Counter] = ITEM_QUEUE;
//			}
//
//			UART_WriteBlocking(base2, (void *) BUFF, sizeof(BUFF));
////
//
//		}
//		xBits = xEventGroupWaitBits(BLUETOOTH_EVENTS, ESC | ENT, pdTRUE,
//		pdFALSE, portMAX_DELAY);
//		xEventGroupClearBits(BLUETOOTH_EVENTS, ESC | ENT | DONE);
//		if (base == UART0)
//			vTaskResume(INIT_MENU_HANDLE);
//		if (base == UART3)
//			vTaskResume(INIT_MENU_HANDLE);
//
//	}
//
//}
