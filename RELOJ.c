/*
 * RELOJ.c
 *
 *  Created on: 15/04/2017
 *      Author: esteban
 */

#include <string.h>
#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "I2CMEM.h"
#include "TASK_CONTROL.h"
#include "CONFIG.h"
#include "UART.h"
#include "semphr.h"
#include "RELOJ.h"
#include "RANDF.h"

//#define ANIO  	2017  /*!< Range from 1970 to 2099.*/
//#define	MES 	04  /*!< Range from 1 to 12.*/
//#define DIA  	15   /*!< Range from 1 to 31 (depending on month).*/
//#define HORA  	6   /*!< Range from 0 to 23.*/
//#define MINUTO 	28 /*!< Range from 0 to 59.*/
//#define SEGUNDO 35  /*!< Range from 0 to 59.*/
#define RTC_ADDR  (0x6FU)


void RTC_init(void)
{


	/** I2C CONFIG**/
	i2c_master_config_t I2C_CONFIG;
	I2C_CONFIG.enableMaster 	 = true;
	I2C_CONFIG.enableHighDrive 	 = false;
	I2C_CONFIG.enableStopHold 	 = false;
	I2C_CONFIG.baudRate_Bps		 = 100000;  //clock frecuency taken from the 24LC256
	I2C_CONFIG.glitchFilterWidth = 0;

	/** GET I2C FRECUENCY */
	uint32_t I2C_CLK;
	I2C_CLK = CLOCK_GetFreq(I2C1_CLK_SRC);

	/** CLOCK ENABLE FOR I2C */
	CLOCK_EnableClock(kCLOCK_PortC);

	/** Set pins for I2C */
	port_pin_config_t PIN;
	PIN.pullSelect = kPORT_PullUp;
	PIN.slewRate = kPORT_FastSlewRate;
	PIN.passiveFilterEnable = kPORT_PassiveFilterDisable;
	PIN.openDrainEnable = kPORT_OpenDrainEnable;
	PIN.driveStrength = kPORT_LowDriveStrength;
	PIN.mux = kPORT_MuxAlt2;
	PIN.lockRegister = kPORT_UnlockRegister;

	PORT_SetPinConfig(PORTC, 10, &PIN);		/**SCL*/
	PORT_SetPinConfig(PORTC, 11, &PIN);		/**SDA*/

	/** SET I2C INTERRUPT PRIORITY */
	NVIC_SetPriority(I2C1_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY);

	/** INITIALIZE I2C */
	status_t STATUS_I2C;
	//STATUS_I2C = I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);
	STATUS_I2C = 0;//I2C_RTOS_Init(&I2C_TRANSFER_HANDLE, I2C1, &I2C_CONFIG, I2C_CLK);

	i2c_mem_reg = 0x80;
	if (kStatus_Success == STATUS_I2C)
	{
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;  //0x6F
		I2C_TRANSFER.direction = kI2C_Write;
		I2C_TRANSFER.subaddress = 0x00;
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &i2c_mem_reg;		//0x80
	//	I2C_TRANSFER.dataSize = sizeof(i2c_mem_reg);
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay (10);
		STATUS_I2C = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
	}
	if (kStatus_Success != STATUS_I2C) {
		for(;;);
	}
}

void RTC_READ(void *arg) {
	int32_t Counter;
	status_t status;
	uint32_t RTC_SUB_ADDR;
	uint8_t CounterD;
	uint8_t DEC[1], UN[1];
	uint8_t DEC_M[1], UN_M[1];
	uint8_t DEC_H[1], UN_H[1];

	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;

	RTC_SUB_ADDR = 0x00;
	for (Counter = 0; Counter < 6; Counter++) {
		if (BANDERA_H_F == 1 & Counter == 3) {
			RTC_SUB_ADDR++;
		}
		//	RTC_VAL = 0x83;
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
		I2C_TRANSFER.direction = kI2C_Read;
		I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &RTC_VAL;
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay(10);
		status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		vTaskDelay(10);

		/** Error si no se pudo leer */

		if (kStatus_Fail == status) {
			break;
		}

		switch (Counter) {
		case 0:
			if ((BANDERA_H_F == 0)) {
				DEC[0] = ((RTC_VAL >> 4) & 0x7) + 0x30;
				UN[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 1:

			if ((BANDERA_H_F == 0)) {
				DEC_M[0] = ((RTC_VAL >> 4) & 0x7) + 0x30;
				UN_M[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 2:
			if ((BANDERA_H_F == 0)) {
				if (BANDERA_RTC == 1) {
					if ((RTC_VAL & 0x3F) > 12) {
						RTC_VAL = RTC_VAL - 0x12;
						netconn_write(newconn, ("PM"), sizeof("PM"),
								NETCONN_COPY); //Dec_seg
					} else if ((RTC_VAL & 0x3F) < 12)
						netconn_write(newconn, ("AM"), sizeof("AM"),
								NETCONN_COPY); //Dec_seg

					DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
					UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
				}
				if (BANDERA_RTC == 2) {
					DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
					UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
				}
			}
			break;
		case 3:
			if ((BANDERA_H_F == 1)) {
				DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
				UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 4:
			if ((BANDERA_H_F == 1)) {
				DEC_M[0] = ((RTC_VAL >> 4) & 0x1) + 0x30;
				UN_M[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 5:
			if ((BANDERA_H_F == 1)) {
				DEC[0] = ((RTC_VAL >> 4)) + 0x30;
				UN[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		}
		RTC_SUB_ADDR++;

	}
	netconn_write(newconn, (DEC_H), sizeof((DEC_H)), NETCONN_COPY); //Dec_seg
	netconn_write(newconn, (UN_H), sizeof((UN_H)), NETCONN_COPY);//uni_seg
	netconn_write(newconn, ":", sizeof(":"), NETCONN_COPY); //Dec_seg

	netconn_write(newconn, (DEC_M), sizeof((DEC_M)), NETCONN_COPY); //Dec_seg
	netconn_write(newconn, (UN_M), sizeof((UN_M)), NETCONN_COPY);//uni_seg
	netconn_write(newconn, ":", sizeof(":"), NETCONN_COPY); //Dec_seg

	netconn_write(newconn, (DEC), sizeof((DEC)), NETCONN_COPY); //Dec_seg
	netconn_write(newconn, (UN), sizeof((UN)), NETCONN_COPY);	 //uni_seg


}
void RTC_WRITE(void *arg)
{
	int32_t Counter;
	status_t status;
	struct netconn ** conn = (struct netconn**) arg;
	struct netconn * newconn = *conn;

	for(;;)
	{
		for (Counter = 0;Counter<3;Counter++)
		{

			I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
			I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
			I2C_TRANSFER.direction = kI2C_Write;
			I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
			I2C_TRANSFER.subaddressSize = 1;
			I2C_TRANSFER.data = &HORA_BUFFER[Counter];
			I2C_TRANSFER.dataSize = 1;
			vTaskDelay (100);
			status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
			vTaskDelay (100);

			/** Error si no se pudo leer */

			if (kStatus_Fail == status)
			{
				break;
			}


		RTC_SUB_ADDR = RTC_SUB_ADDR + 1;
		}

		break;
	}
}
void RTC_ECO(void) {
	int32_t Counter;
	status_t status;
	uint32_t RTC_SUB_ADDR;
	uint8_t CounterD;
	uint8_t DEC[1], UN[1];
	uint8_t DEC_M[1], UN_M[1];
	uint8_t DEC_H[1], UN_H[1];


	RTC_SUB_ADDR = 0x00;
	for (Counter = 0; Counter < 6; Counter++) {
		if (BANDERA_H_F == 1 & Counter == 3) {
			RTC_SUB_ADDR++;
		}
		//	RTC_VAL = 0x83;
		I2C_TRANSFER.flags = kI2C_TransferDefaultFlag;
		I2C_TRANSFER.slaveAddress = RTC_ADDR;      //0x6F
		I2C_TRANSFER.direction = kI2C_Read;
		I2C_TRANSFER.subaddress = RTC_SUB_ADDR;   //0x00
		I2C_TRANSFER.subaddressSize = 1;
		I2C_TRANSFER.data = &RTC_VAL;
		I2C_TRANSFER.dataSize = 1;
		vTaskDelay(10);
		status = I2C_RTOS_Transfer(&I2C_TRANSFER_HANDLE, &I2C_TRANSFER);
		vTaskDelay(10);

		/** Error si no se pudo leer */

		if (kStatus_Fail == status) {
			break;
		}

		switch (Counter) {
		case 0:
			if ((BANDERA_H_F == 0)) {
				DEC[0] = ((RTC_VAL >> 4) & 0x7) + 0x30;
				UN[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 1:

			if ((BANDERA_H_F == 0)) {
				DEC_M[0] = ((RTC_VAL >> 4) & 0x7) + 0x30;
				UN_M[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 2:
			if ((BANDERA_H_F == 0)) {
				if (BANDERA_RTC == 1) {
					if ((RTC_VAL & 0x3F) > 12) {
						RTC_VAL = RTC_VAL - 0x12;
						LCDNokia_sendString('PM');
					} else if ((RTC_VAL & 0x3F) < 12)
						LCDNokia_sendString('AM');

					DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
					UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
				}
				if (BANDERA_RTC == 2) {
					DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
					UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
				}
			}
			break;
		case 3:
			if ((BANDERA_H_F == 1)) {
				DEC_H[0] = ((RTC_VAL >> 4) & 0x3) + 0x30;
				UN_H[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 4:
			if ((BANDERA_H_F == 1)) {
				DEC_M[0] = ((RTC_VAL >> 4) & 0x1) + 0x30;
				UN_M[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		case 5:
			if ((BANDERA_H_F == 1)) {
				DEC[0] = ((RTC_VAL >> 4)) + 0x30;
				UN[0] = (RTC_VAL & 0x0F) + 0x30;
			}
			break;
		}
		RTC_SUB_ADDR++;

	}

	LCDNokia_init();
	LCDNokia_clear();
//	LCDNokia_gotoXY(0x10, 0x10);
//	vTaskDelay(100);


	uint8_t strg[7];
	strg[0] = DEC_H[0];
	strg[1]	= UN_H[0];
	strg[2] = ':';
	strg[3] = DEC_M[0];
	strg[4] = UN_M[0];
	strg[5] = ':';
	strg[6] = DEC[0];
	strg[7] = UN[0];
	strg[8] = 0;

	LCDNokia_sendString(strg);
	vTaskDelay(100);

/*
//	LCD_delay();
	LCDNokia_sendString(DEC_H);
//	LCD_delay();
	LCDNokia_sendString(UN_H);
//	LCD_delay();
	LCDNokia_sendString(':');
//	LCD_delay();
	LCDNokia_sendString(DEC_M);
//	LCD_delay();
	LCDNokia_sendString(UN_M);
	LCD_delay();
	LCDNokia_sendString(':');
	LCD_delay();
	LCDNokia_sendString(DEC);
	LCD_delay();
	LCDNokia_sendString(UN);
	LCD_delay();*/
}
