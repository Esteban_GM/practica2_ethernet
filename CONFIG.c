/*
 * CONFIG.c
 *
 *  Created on: 04/04/2017
 *      Author: esteban
 */

#include <string.h>

#include "board.h"
#include "pin_mux.h"
#include "clock_config.h"
/*#include "fsl_debug_console.h"*/

/* FreeRTOS kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"

#include "CONFIG.h"
#include "fsl_uart.h"
#include "fsl_port.h"
#include "fsl_uart_freertos.h"
#include "task.h"
#include "UART.h"
#include "I2CMEM.h"


/** Configura los componentes a utilizar.   De esta manera se evita llamarlos mas tarde**/
void Inicializacion(void)
{
	BOARD_InitPins();
	BOARD_BootClockRUN();
	BOARD_InitDebugConsole();

	UART_TERATERM_INIT();
	UART_BLUETOOTH_INIT();
//	I2C_INIT();
	//RTC_init();

//	clock_config();
}

/** Create events, queues, semaphores... */

void Q_E_S_Creation(void){
	TERATERM_EVENTS = xEventGroupCreate();
	TERATERM_QUEUE  = xQueueCreate(100, sizeof(char));	/** * Creates a new queue instance, and returns a handle by which the new queue can be referenced.**/

	BLUETOOTH_EVENTS = xEventGroupCreate();
	BLUETOOTH_QUEUE  = xQueueCreate(100, sizeof(char));	/** * Creates a new queue instance, and returns a handle by which the new queue can be referenced.**/

}
